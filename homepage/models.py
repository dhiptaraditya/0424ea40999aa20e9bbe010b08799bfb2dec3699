from django.db import models

# Create your models here.

class MainPage(models.Model):
	description = models.TextField(max_length=9389)

class Opportunitie(models.Model):
	title = models.CharField(max_length=100)

class Tag(models.Model):
	name = models.CharField(max_length=50)
	opportunities = models.ManyToManyField(Opportunitie)
class NewTag(models.Model):
	name = models.CharField(max_length=50)