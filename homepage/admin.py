from django.contrib import admin
from .models import Opportunitie, Tag, MainPage, NewTag
# Register your models here.
admin.site.register(Opportunitie)
admin.site.register(Tag)
admin.site.register(NewTag)
admin.site.register(MainPage)
