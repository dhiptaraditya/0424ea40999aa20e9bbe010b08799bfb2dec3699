from django.shortcuts import render
from .models import Opportunitie, NewTag, MainPage
# Create your views here.
def index(request):	
	dataTag = NewTag.objects.all()
	mainDesc = MainPage.objects.get(id=1)
	opportunity = Opportunitie.objects.all()

	return render(request, 'index.html', {'dataOps':opportunity, 'dataTag':dataTag, 'desc':mainDesc})